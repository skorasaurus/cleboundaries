cleboundaries
=============

City of Cleveland ward boundaries as of January 1, 2014. 

Inspired by: 

http://project.wnyc.org/nyc-districting-revised/index.html?lat=40.7370&lon=-73.9220&zoom=12

contents: 

2014wardboundaries.osm
Boundaries were revised and passed by City Council in April 2014. 

You can obtain the proposed boundaries that were publically released in March 2013 by viewing my commit history and using the 2014wardboundaries-proposed.osm file. 

2014wardboundaries.shp 
Same as 2014wardboundaries.osm as a SHP. 

2014wardboundaries.geojson
Same as 2014wardboundaries.osm as geojson. 

boundaries-proposed
- tilemill project of the styled ward boundaries
To reproduce on your computer, you can simply change the layer source from a POSTGIS database (generated by osm2pgsql) that I had used, to the geojson or shp included in this repo. 

index.html 
- simple html page using mapbox.js/leaflet that displays the borders over a mapbox basemap. 

License: 
WTFPL